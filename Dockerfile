FROM centos:latest

FROM anewhuahua/mysql5.6

FROM scomm/php5.6

FROM dustise/yum.centos7

RUN yum -y install httpd

CMD /usr/sbin/httpd -D FOREGROUND

VOLUME /var/www/html/
